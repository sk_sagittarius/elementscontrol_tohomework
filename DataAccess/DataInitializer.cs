﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Services;

namespace DataAccess
{
    //public class DataInitializer : DropCreateDatabaseAlways<SecurityContext>
    public class DataInitializer : CreateDatabaseIfNotExists<SecurityContext>
    {
        protected override void Seed(SecurityContext context)
        {
            context.Users.Add(new Models.User
            {
                Login = "admin",
                Password = DataEncryptor.HashPassword("123")
                //Password = "123ASD" // 1a2d3mAiSnD - пример легкой шифровки
                // password + соль - вариант посложнее -> data encryptor
            });
        }
    }
}
