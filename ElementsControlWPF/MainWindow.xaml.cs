﻿using DataAccess;
using Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ElementsControlWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SignInButtonClick(object sender, RoutedEventArgs e)
        {
            var login = loginTextBox.Text;
            var password = passwordBox.Password;

            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Fill all fields");
                return;
            }

            using (var context = new SecurityContext())
            {
                var user = context.Users.FirstOrDefault(searchingUser => searchingUser.Login == login);
                // password не сравниваем, так как он зашифрован
                if(user!=null && DataEncryptor.IsValidPassword(password, user.Password))
                {
                    MessageBox.Show("Welcome!");
                }
                else
                {
                    MessageBox.Show("Nope!");
                }
            }
        }

        private void RegistrationButtonClick(object sender, RoutedEventArgs e)
        {
            var login = loginRegistrationTextBox.Text.ToLower();
            var password = passwordRegistrationBox.Password;

            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Fill all fields");
                return;
            }

            using (var context = new SecurityContext())
            {
                User user = new User();
                if(context.Users.Any(searchingUser => searchingUser.Login == login))
                {
                    MessageBox.Show("Пользователь с таким логином уже существует!");
                }
                else
                {
                    user.Login = login;
                    user.Password = DataEncryptor.HashPassword(password);
                    context.Users.Add(user);
                    context.SaveChanges();
                    MessageBox.Show("Пользователь добавлен!");
                }
            }
        }
    }
}
